(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("e16cd3e6093cf35e65dbac6c7649931936f32f56be34477cb7cbe1ee332c5b99" default))
 '(org-agenda-files
   '("/home/oscar/Documents/org-files/agenda.org" "/home/oscar/Documents/org-roam/inbox.org"))
 '(package-selected-packages
   '(nerd-icons nerd-icons-completion nerd-icons-dired nerd-icons-ibuffer nerd-icons-ivy-rich smartparens ef-themes magit-stats mu4easy discover emojify mastodon telega rainbow-identifiers mermaid-mode ob-mermaid jedi ein cdlatex oblivion-theme helpful-report mu4e gnuplot academic-phrases academic-frases ob-sagemath sage-shell-mode ivy-prescient org-noter-pdftools org-pdftools org-pdfview helm-bibtex org-tree-slide hide-mode-line focus diffpdf calibredb nov elfeed writeroom-mode writegood-mode erc-image erc-hl-nicks counsel-projectile projectile org-noter org-roam-bibtex yasnippet-snippets which-key use-package transpose-frame switch-window py-autopep8 promise pretty-mode pdf-tools ox-reveal org-sticky-header org-roam-ui org-ref org-inline-pdf org-elp org-download org-bullets oauth2 mustache magithub key-chord ivy-bibtex hungry-delete elpy djvu dired-subtree dired-narrow dired-k diminish diff-hl deft dashboard counsel company-auctex command-log-mode calfw-org calfw auto-compile auctex-latexmk))
 '(sage-shell:use-prompt-toolkit nil)
 '(sage-shell:use-simple-prompt t)
 '(send-mail-function 'mailclient-send-it)
 '(warning-suppress-log-types '((:warning)))
 '(warning-suppress-types '((org-element-cache))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bold ((t (:foreground "red" :weight bold))))
 '(italic ((t (:foreground "purple" :weight bold))))
 '(org-document-info ((t (:height 1.1 :underline nil))))
 '(org-document-info-keyword ((t (:height 1.1 :underline nil))))
 '(org-document-title ((t (:height 1.1 :underline nil))))
 '(org-level-1 ((t (:inherit default :weight bold :height 1.5))))
 '(org-level-2 ((t (:inherit default :weight bold :height 1.3))))
 '(org-level-3 ((t (:inherit default :weight bold :height 1.15))))
 '(org-level-4 ((t (:inherit default :weight bold :height 1.1))))
 '(org-meta-line ((t (:height 0.9 :underline nil))))
 '(underline ((t (:foreground "yellow2" :weight bold)))))
