(require 'use-package-ensure)
(setq use-package-always-ensure t)

(use-package auto-compile
  :config (auto-compile-on-load-mode))

(setq load-prefer-newer t)

;; (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(setq user-full-name "Oscar Castillo-Felisola"
      user-mail-address "o.castillo.felisola@protonmail.com"
      calendar-latitude -33.66
      calendar-longitude -71.51
      calendar-location-name "Valparaiso, CHILE")

(set-frame-parameter (selected-frame) 'alpha '(95 . 95))
(add-to-list 'default-frame-alist '(alpha . (95 . 95)))

(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(if (string< emacs-version
             "26.3")
    (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

(setq gc-cons-threshold 20000000)

(setq make-backup-files nil)

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(setq vc-follow-symlinks t)

(setq sentence-end-double-space nil)

(setq confirm-kill-emacs 'y-or-n-p)

(put 'dired-find-alternate-file 'disabled nil)

(setq-default dired-listing-switches "-alh")

(setq dired-recursive-copies 'always)

(use-package dired-subtree
  :ensure t
  :after dired
  :bind (:map dired-mode-map
              ("TAB" . dired-subtree-toggle)))

(use-package dired-narrow
  :ensure t
  :bind (:map dired-mode-map
              ("/" . dired-narrow)))
;; (require 'dired)
;; (define-key dired-mode-map (kbd "/") 'dired-narrow-fuzzy)

(fset 'yes-or-no-p 'y-or-n-p)

(global-auto-revert-mode t)

(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)

(setq display-time-24hr-format t)
(setq display-time-format " %H:%M - %d %B %Y ")
(setq display-time-default-load-average nil)

(display-time-mode 1)

(tool-bar-mode 0)

(set-fringe-style 10)

(add-hook 'org-mode-hook 'auto-fill-mode)

(put 'narrow-to-region 'disabled nil)

(scroll-bar-mode -1)

(add-hook 'eww-mode-hook 'scroll-lock-mode)

(setq visible-bell t)

(use-package pretty-mode
  :config
  (global-pretty-mode t)

  (pretty-deactivate-groups
   '(:equality :ordering :ordering-double :ordering-triple
               :arrows :arrows-twoheaded :punctuation
               :logic :sets))

  (pretty-activate-groups
   '(:sub-and-superscripts :greek :arithmetic-nary)))

;; The following is built-in Emacs
(global-prettify-symbols-mode t)

;; (when window-system
;;   (global-hl-line-mode 1))

;; (use-package inconsolata
;;   :ensure-system-package
;;   (inconsolata . "sudo apt-get install fonts-inconsolata"))

(setq ocf/default-font "Inconsolata")
(setq ocf/default-font-size 18)
(setq ocf/current-font-size ocf/default-font-size)

(setq ocf/font-change-increment 1.1)

(defun ocf/set-font-size ()
  "Set the font to `ocf/default-font' at `ocf/current-font-size'."
  (set-frame-font
   (concat ocf/default-font "-" (number-to-string ocf/current-font-size))))

(defun ocf/reset-font-size ()
  "Change font size back to `ocf/default-font-size'."
  (interactive)
  (setq ocf/current-font-size ocf/default-font-size)
  (ocf/set-font-size))

(defun ocf/increase-font-size ()
  "Increase current font size by a factor of `ocf/font-change-increment'."
  (interactive)
  (setq ocf/current-font-size
	(ceiling (* ocf/current-font-size ocf/font-change-increment)))
  (ocf/set-font-size))

(defun ocf/decrease-font-size ()
  "Decrease current font size by a factor of `ocf/font-change-increment', down to a minimum size of 1."
  (interactive)
  (setq ocf/current-font-size
	(max 1
	     (floor (/ ocf/current-font-size ocf/font-change-increment))))
  (ocf/set-font-size))

(define-key global-map (kbd "C-)") 'ocf/reset-font-size)
(define-key global-map (kbd "C-x C-+") 'ocf/increase-font-size)
(define-key global-map (kbd "C-x C--") 'ocf/decrease-font-size)

(ocf/reset-font-size)

(use-package diff-hl
  :config
  (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'org-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-package oblivion-theme)
(load-theme 'oblivion t)
;; (load-theme 'deeper-blue t)
;; (load-theme 'misterioso t)
;; (load-theme 'leuven t)

(defvar base-frame-title-format nil
"Like frame-title-format to be used as a base for to modify it by")

(setq base-frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))
        (:eval (if (buffer-modified-p)
                   " •"))
        " — Emacs"))

;; (defun clock-in-frame-title ()
;;   (if (org-clocking-p)
;;       (setq frame-title-format (list  base-frame-title-format
;;                                       " — 🕓: "
;;                                       (org-clock-get-clock-string)
;;                                       " — ⟳:"
;;                                       org-timer-mode-line-string))
;;     (setq frame-title-format base-frame-title-format)))
;; (run-at-time t 1 'clock-in-frame-title)
;; (add-hook 'org-clock-in-hook 'clock-in-frame-title)
;; (add-hook 'org-clock-out-hook 'clock-in-frame-title)
;; (add-hook 'org-clock-cancel-hook 'clock-in-frame-title)

(use-package smartparens
  :ensure t)
(require 'smartparens-config)

(add-to-list 'auto-mode-alist '("\\.odt\\'" . doc-view-mode))

(setq org-clock-sound "/home/oscar/Music/mixkit-achievement-bell-600.wav")

(setq org-completion-use-ido t)
(setq ido-everywhere t)
(setq ido-max-directory-size 100000)
(ido-mode (quote both))
; Use the current window when visiting files and buffers with ido
(setq ido-default-file-method 'selected-window)
(setq ido-default-buffer-method 'selected-window)
; Use the current window for indirect buffer display
(setq org-indirect-buffer-display 'current-window)

;; (global-set-key (kbd "C-x b") 'ibuffer)

(use-package which-key
  :diminish which-key-mode
  :config
  (which-key-mode))

(use-package avy
  :bind
  ("M-s" . avy-goto-char))

(use-package async
  :init (dired-async-mode 1))

(use-package diminish
  :init
  (diminish 'which-key)
  (diminish 'auto-revert-mode)
  (diminish 'flyspell-mode)
  (diminish 'auto-fill-mode)
  (diminish 'subword-mode)
  (diminish 'org-indent-mode)
  (diminish 'hungry-delete-mode))

(use-package switch-window
  :config
    (setq switch-window-input-style 'minibuffer)
    (setq switch-window-increase 4)
    (setq switch-window-threshold 2)
    (setq switch-window-shortcut-style 'qwerty)
    (setq switch-window-qwerty-shortcuts
          '("a" "s" "d" "f" "j" "k" "l" "i" "o"))
    (setq switch-window-minibuffer-shortcut ?z)
    
  :bind
    ([remap other-window] . switch-window))

(use-package dashboard
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-startup-banner "~/.emacs.d/science_icon_01.png")
  (setq dashboard-items '((recents . 9)
			  (projects . 9))))

(use-package yasnippet
  :ensure t

  :config
  (use-package yasnippet-snippets
    :ensure t)
  (yas-reload-all))

(yas-global-mode 1)

(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
(define-key yas-minor-mode-map (kbd "C-<tab>") 'yas-expand)

(ispell-change-dictionary "british" t)

(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))
(add-hook 'org-mode-hook (lambda () (setq ispell-parser 'tex)))

(setq explicit-shell-file-name "/bin/zsh")
(define-key global-map (kbd "<f10>") 'shell)

(define-key global-map (kbd "RET") 'newline-and-indent)
(setq-default indent-tabs-mode nil)

(use-package counsel)

(use-package ivy
  :bind
  ("C-x b" . counsel-switch-buffer)
  ;; ("C-x C-f" . counsel-find-file)
  ("C-s" . swiper-isearch)
  ("C-S-s" . isearch-forward)
  ("M-x" . counsel-M-x)
  ("C-c m" . counsel-linux-app)
  ("C-c o" . counsel-outline)
  :config
  (use-package ivy-prescient
    :init
    (ivy-prescient-mode t))
  (setq ivy-use-virtual-buffers t)
  (setq ivy-initial-inputs-alist nil)
  (setq ivy-count-format "(%d/%d) ")
  )

;; (use-package all-the-icons-ivy-rich
;;   :ensure t
;;   :init (all-the-icons-ivy-rich-mode 1))

(use-package ivy-rich
  :ensure t
  :init (ivy-rich-mode 1))

(transient-mark-mode 1)
(show-paren-mode 1)
(setq show-paren-style 'expression)
(electric-pair-mode t)
(setq electric-pair-skip-self t)

(setq-default truncate-lines nil)
;; Even for org-mode
;; (setq org-startup-truncated nil)

(define-key global-map "\C-x\t" 'pcomplete)

(use-package company
  :ensure t)

(setq company-idle-delay 0)
(setq company-minimum-prefix-length 3)
(add-hook 'after-init-hook 'global-company-mode)

(setq company-quickhelp-idle-delay 1)

(use-package company-auctex
  :ensure t

  :config
  (company-auctex-init))

(set-register ?c '(file . "~/Software/git.src/mydotfiles/emacs.d/configuration2.org"))
(set-register ?i '(file . "~/Documents/org-roam/inbox.org"))
(set-register ?a '(file . "~/Documents/org-files/agenda.org"))

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'turn-on-auto-fill)

(global-set-key (kbd "C-c q") 'auto-fill-mode)

(defun ocf/unfill-paragraph ()
    "Takes a multi-line paragraph and makes it into a single line of text."
    (interactive)
    (let ((fill-column (point-max)))
      (fill-paragraph nil)))

(defun ocf/toggle-fill-paragraph ()
  "Toggle fill paragraph Version 2016-09-20"
  (interactive)
  ;; use a property “state”. Value is t or nil
  (if (get 'ocf/toggle-fill-paragraph 'state)
      (progn (ocf/unfill-paragraph)
             (put 'ocf/toggle-fill-paragraph 'state nil))
    (progn
      (fill-paragraph)
      (put 'ocf/toggle-fill-paragraph 'state t))))

(global-set-key (kbd "M-q") 'ocf/toggle-fill-paragraph)

;; (require 'dired+)

(use-package dired-k)
(define-key dired-mode-map (kbd "K") 'dired-k)

;; You can use dired-k alternative to revert-buffer
(define-key dired-mode-map (kbd "g") 'dired-k)

;; always execute dired-k when dired buffer is opened
(add-hook 'dired-initial-position-hook 'dired-k)

(add-hook 'dired-after-readin-hook #'dired-k-no-revert)

(mapc (lambda (x)
        (add-to-list 'completion-ignored-extensions x))
      '(".aux" ".bbl" ".blg" ".exe"
        ".log" ".meta" ".out" ".pyg"
        ".synctex.gz" ".tdo" ".toc"
        "-pkg.el" "_latexmk" ".fls"))

(defun ocf/inline-eq ()
  "Insert the LaTeX inline equation delimiter"
  (interactive)
  (if (use-region-p)
      (progn
        (let ((p1 (region-beginning))
              (p2 (region-end)))
          (goto-char p2)
          (insert "\\)")
          (goto-char p1)
          (insert "\\(")))
    (progn (insert "\\(\\)") (backward-char 2))))

(global-set-key (kbd "C-¿") 'ocf/inline-eq)
(global-set-key (kbd "C-=") 'ocf/inline-eq)

(defun ocf/change-LaTeX-inline-delimiter ()
  "Change the dollar sign denoting LATeX inline equation by the '\(...\)' delimiter. USAGE: Place the point somewhere between the delimiter and call the function."
  (interactive)
  (skip-chars-backward "^\$")
  (delete-char -1)
  (insert "\\(")
  (skip-chars-forward "^\$")
  (delete-char 1)
  (insert "\\)"))

(global-set-key (kbd "C--") 'ocf/change-LaTeX-inline-delimiter)

(fset 'ocf/displaymode
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([92 91 return return 92 93 16] 0 "%d")) arg)))

(global-set-key (kbd "C-¡") 'ocf/displaymode)

(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)

(defun config-visit ()
  "Finds the configuration file ~/.emacs.d/configuration2.org at runtime"
  (interactive)
  (find-file "~/.emacs.d/configuration2.org"))
(global-set-key (kbd "C-c e") 'config-visit)

(defun config-reload ()
  "Reloads ~/.emacs.d/configuration2.org at runtime"
  (interactive)
  (org-babel-load-file (expand-file-name "~/.emacs.d/configuration2.org"))
  (load-file (expand-file-name "~/.emacs.d/custom.el")))
(global-set-key (kbd "C-c r") 'config-reload)

(use-package hungry-delete
  :ensure t

  :diminish
  hungry-delete-mode
  
  :config
  (global-hungry-delete-mode t))

(use-package transpose-frame
  :ensure t)

(use-package nerd-icons
  :ensure t)

(use-package nerd-icons-dired
  :ensure t
  :hook
  (dired-mode . nerd-icons-dired-mode)
  )

(use-package command-log-mode)

(use-package helpful)

(defun ocf/buffer-name ()
  "I want to get the buffer name"
  (interactive)
  (let ((my-string (buffer-name)))
    (message "%s" my-string)
    (kill-new my-string)))

(use-package gnuplot
  :ensure t)

;; (setenv "WORKON_HOME" "~/Software/anaconda3/envs")
;; (pyvenv-mode 1)

;; (setq python-indent 4)

(use-package elpy
  :init
  (elpy-enable))
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt")

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(use-package py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-mode)

(use-package jedi)

(add-hook 'sh-mode-hook
          (lambda ()
            (setq sh-basic-offset 2
                  sh-indentation 2)))

(setq latex-block-names '("theorem" "corollary" "proof"
                          "frame" "block" "alertblock"
                          "definition" "example" "align"
                          "align*" "columns" "tikzpicture"
                          "axis" "cases" "matrix" "pmatrix"
                          "vmatrix" "parts" "questions"
                          "solution" "Ebox" "WEbox" "widetext"
                          "dmath" "dmath*" "split" "cdbexample"
			  "cdbexample*"))

(setq ob-mermaid-cli-path "/usr/bin/mmdc")

(use-package magit
  :bind
  ("C-x g" . magit-status))

;; (use-package magithub)

(use-package magit-stats
  :ensure t
  :config
  (require 'magit-stats))

(use-package pdf-tools)
(pdf-tools-install)

;; (setq auto-revert-interval 0.5)
;; (auto-revert-set-timer)

(add-hook 'pdf-view-hook 'auto-revert-mode)

(define-key pdf-view-mode-map (kbd "h") 'pdf-annot-add-highlight-markup-annotation)
(define-key pdf-view-mode-map (kbd "t") 'pdf-annot-add-text-annotation)
(define-key pdf-view-mode-map (kbd "D") 'pdf-annot-delete)

(setq pdf-view-resize-factor 1.1)

(use-package org
  :pin gnu
  :bind
  ("C-c a" . org-agenda)
  ("C-c c" . org-capture)
  ("C-c l" . org-store-link)

  :hook ((org-mode . org-bullets-mode)
         (org-babel-after-execute . org-display-inline-images)
         (org-mode . org-cdlatex-mode)
         (org-mode . abbrev-mode)
         )
  :custom-face
  ;; (org-document-title ((t (:weight bold :height 1.5))))
  ;; (org-done ((t (:foreground "red"  :strike-through t :weight bold))))
  ;; (org-headline-done ((t (:strike-through t))))
  ;; (org-level-1 ((t (:foreground "DarkOliveGreen1" :weight bold :height 1.3))))
  ;; (org-level-2 ((t (:foreground "DarkOliveGreen2" :weight normal :height 1.2))))
  ;; (org-level-3 ((t (:foreground "DarkOliveGreen3" :weight normal :height 1.1))))

  :config
  (setq-default org-highlight-latex-and-related '(native script entities)
                org-pretty-entities t
                org-pretty-entities-include-sub-superscripts t)
  )

(if window-system
  (let* ((headline `(:inherit default :weight bold)))
    (custom-theme-set-faces
     'user
     `(org-level-4 ((t (,@headline :height 1.1))))
     `(org-level-3 ((t (,@headline :height 1.15))))
     `(org-level-2 ((t (,@headline :height 1.3))))
     `(org-level-1 ((t (,@headline :height 1.5))))
     `(org-document-info-keyword ((t (:height 1.1 :underline nil))))
     `(org-document-info         ((t (:height 1.1 :underline nil))))
     `(org-meta-line             ((t (:height 0.9 :underline nil))))
     `(org-document-title        ((t (:height 1.1 :underline nil))))))
)

(use-package "org-sticky-header"
  :ensure t
  :init
   (progn
;     (setq org-sticky-header-full-path 'full)
     (setq org-sticky-header-full-path nil)
     (setq org-sticky-header-heading-star " ")
     (setq org-sticky-header-always-show-header nil)
;     (setq org-sticky-header-always-show-header t)
     (add-hook 'org-mode-hook 'org-sticky-header-mode)
    )
  )

(setq org-use-speed-commands t)

;; (add-to-list 'org-speed-commands (cons "m" 'org-mark-subtree))
;; (add-to-list 'org-speed-commands-user (cons "P" 'org-set-property))
;; (add-to-list 'org-speed-commands-user (cons "d" 'org-deadline))

(setq org-indirect-buffer-display 'current-window)
(setq org-startup-indented nil)
(setq org-src-preserve-indentation nil)
(setq org-edit-src-content-indentation 0)
;; (setq org-src-tab-acts-natively t)

(setq org-highlight-latex-and-related '(latex))

(defun my-org-latex-yas ()
  "Activate org and LaTeX yas expansion in org-mode buffers."
  (interactive)
  ;; (yas-minor-mode)
  (yas-activate-extra-mode 'latex-mode))

(add-hook 'org-mode-hook #'my-org-latex-yas)

(defun org-latex-format-headline-colored-keywords-function
    (todo todo-type priority text tags info)
        (concat
           (cond ((string= todo "TODO")(and todo (format "{\\color{red}\\bfseries\\sffamily %s} " todo)))
   ((string= todo "DONE")(and todo (format "{\\color{green}\\bfseries\\sffamily %s} " todo))))
            (and priority (format "\\framebox{\\#%c} " priority))
            text
            (and tags
            (format "\\hfill{}\\textsc{%s}"
    (mapconcat (lambda (tag) (org-latex-plain-text tag info))
           tags ":")))))

(setq org-latex-format-headline-function 'org-latex-format-headline-colored-keywords-function)

(setq org-use-fast-todo-selection t)

(setq org-todo-keywords     
      '((sequence "TODO(t)" "STARTED(s!)" "NEXT(n)" "TASK(k)" "FEEDBACK(f@/!)" "VERIFY(v)" "WAITING(w@/!)" 
                  "|" "DONE(d)" "DELEGATED(l@/!)" "CANCELLED(c@/!)")))

(defun org-icons ()
  "Beautify org mode keywords."
  (setq prettify-symbols-alist '(("TODO" . "")
                                 ("STARTED" . "")
	                         ("WAITING" . "")        
   				 ("NOPE" . "")      
   				 ("CANCELLED" . "")
				 ("DONE" . "")
				 ("[#A]" . "")
				 ("[#B]" . "")
 				 ("[#C]" . "")
				 ("[ ]" . "")
				 ("[X]" . "")
				 ("[-]" . "")
				 ("#+BEGIN_SRC" . "")
				 ("#+END_SRC" . "―")
				 ("#+begin_src" . "")
				 ("#+end_src" . "―")
				 (":PROPERTIES:" . "")
				 (":END:" . "―")
				 (":properties:" . "")
				 (":end:" . "―")
				 ("#+STARTUP:" . "")
				 ("#+startup:" . "")
				 ("#+RESULTS:" . "")
				 ("#+results:" . "")
				 ("#+NAME:" . "")
				 ("#+name:" . "")
				 ("#+ROAM_TAGS:" . "")
				 ("#+roam_tags:" . "")
				 ("#+FILETAGS:" . "")
				 ("#+filetags:" . "")
				 ("#+HTML_HEAD:" . "")
				 ("#+latex_header:" . "")
				 ("#+TITLE: " . "")
				 ("#+title: " . "")
				 ("#+SUBTITLE:" . "")
				 ("#+AUTHOR:" . "")
				 ("#+author:" . "")
				 ("#+date:" . "")
				 (":Effort:" . "")
				 ("SCHEDULED:" . "")
				 ("DEADLINE:" . "")))
  (prettify-symbols-mode))
(add-hook 'org-mode-hook 'org-icons)

(setq org-archive-location (concat "~/Documents/org-files/archive.org" "::* From %s"))

(use-package org-bullets
  :init
  (add-hook 'org-mode-hook 'org-bullets-mode)
  
  :config                                                   
  (setq org-bullets-bullet-list '("♳" "♴" "♵" "♶" "♷" "♸" "♹" "♺"))
  ;; (setq org-bullets-bullet-list '("◉" "◎" "⚫" "○" "►" "◇"))
  (setq org-hide-leading-stars t))

;; (setq org-ellipsis "⤵")
;; (setq org-ellipsis " ▾ ")
(setq org-ellipsis " [+]")

(setq org-src-fontify-natively t)

(setq org-src-window-setup 'current-window)

;; (require 'ob-ipython)
(org-babel-do-load-languages 'org-babel-load-languages 
			     '((C . t)
			       (ditaa . t)
                               (ein . t)
			       (emacs-lisp . t)
                               (dot . t)
			       (fortran . t)
			       (gnuplot . t)
			       ;; (ipython . t)
			       (latex . t)
			       ;; (ledger . t)
			       ;; (mathematica . t)
			       (maxima . t)
                               (mermaid . t)
			       (octave . t)
			       (org . t)
			       (python . t)
			       (R . t)
                               (scheme . t)
			       (shell . t)
			       ))

(setq org-confirm-babel-evaluate nil)

;; (setq org-src-block-faces '(("emacs-lisp" (:background "gainsboro"))
;; 			    ;; ("ipython" (:background "OliveDrab3"))
;; 			    ("python" (:background "LemonChiffon1"))
;; 			    ("latex" (:background "DimGrey"))
;; 			    ;; ("latex" (:background "PowderBlue"))
;; 			    ("shell" (:background "khaki2"))))
;; (setq org-src-block-faces '(("emacs-lisp" (:background "DarkSlateGrey"))
;; 			    ("ipython" (:background "DarkOrange4"))
;; 			    ("python" (:background "DarkBlue"))
;; 			    ("latex" (:background "MidnightBlue"))
;; 			    ("shell" (:background "DarkGreen"))))

(define-derived-mode cadabra-mode python-mode "cadabra"
  ; make #a symbol constituent
  (modify-syntax-entry ?# "_" cadabra-mode-syntax-table))

(setq org-log-done 'note)

(setq org-clock-continuously nil)

(setq org-agenda-files (list "~/Documents/org-files/agenda.org"
                             "~/Documents/org-roam/inbox.org"
                             ;; "~/Documents/org-files2/personal.org"
                             ;; "~/org/school.org" 
                             ;; "~/org/home.org"
))

(setq org-agenda-span 'fortnight)

(setq org-agenda-start-on-weekday nil)

(setq org-deadline-warning-days 3)

(setq org-startup-with-inline-images "inlineimages")

(setq org-image-actual-width '(700))

;; (add-hook 'org-babel-after-execute-hook
;; 	  'org-display-inline-images)

(use-package org-inline-pdf
  :ensure t
  :config
  (add-hook 'org-mode-hook #'org-inline-pdf-mode))

;; Suggested on the org-mode maillist by Julian Burgos
(add-to-list 'image-file-name-extensions "pdf")
(add-to-list 'image-file-name-extensions "eps")

(add-to-list 'image-type-file-name-regexps '("\\.eps\\'" . imagemagick))
(add-to-list 'image-file-name-extensions "eps")
(add-to-list 'image-type-file-name-regexps '("\\.pdf\\'" . imagemagick))
(add-to-list 'image-file-name-extensions "pdf")

(setq imagemagick-types-inhibit (remove 'PDF imagemagick-types-inhibit))

(use-package ox-reveal)

(setq org-reveal-root "file:///home/oscar/Software/git.src/reveal.js")

(setq org-latex-prefer-user-labels t)

;; avoid getting \maketitle right after begin{document}
;; you should put \maketitle if and where you want it.
(setq org-latex-title-command "")

(require 'ox)
(require 'ox-latex)
(setq org-latex-create-formula-image-program 'dvipng)
;; (setq org-preview-latex-process-alist 'imagemagick)

;; (setq org-latex-listings t)
(setq org-latex-listings 'minted)
;; (add-to-list 'org-latex-packages-alist '("" "minted")) ; When needed I should add it manually
;; (add-to-list 'org-latex-packages-alist '("" "xcolor"))
;; (add-to-list 'org-latex-packages-alist '("" "tikz" t))
(setq org-latex-listings-langs
      (quote ((emacs-lisp "Lisp")
              (lisp "Lisp")
              (clojure "Lisp")
              (c "C")
              (cc "C++")
              (fortran "fortran")
              (perl "Perl")
              (cperl "Perl")
              (python "Python")
              (ruby "Ruby")
              (html "HTML")
              (xml "XML")
              (tex "TeX")
              (latex "[LaTeX]TeX")
              (shell-script "bash")
              (gnuplot "Gnuplot")
              (ocaml "Caml")
              (caml "Caml")
              (sql "SQL")
              (sqlite "sql")
              (R-mode "R"))))

(eval-after-load "preview"
  '(add-to-list 'preview-default-preamble "\\PreviewEnvironment{tikzpicture}" t))

(setq org-latex-pdf-process
      (list "latexmk -shell-escape -bibtex -f -pdf %f"
            "latexmk -c %f"))

(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.5))

(defun my/org-latex-format-headline-function
    (todo _todo-type priority text tags _info)
  "Modified format function for a headline: enclose tags in boxes.
   See `org-latex-format-headline-function' for details."
  (concat
   (and todo (format "{\\bfseries\\sffamily %s} " todo))
   (and priority (format "\\framebox{\\#%c} " priority))
   text
   (and tags
        (format "\\hfill{}\\textsc{%s}"
                (mapconcat (lambda (x) (format "\\framebox{\\tiny %s}" (org-latex--protect-text x))) tags ":")))))

(setq org-latex-format-headline-function #'my/org-latex-format-headline-function)

(require 'ox-latex)

(add-to-list 'org-latex-classes
	     '("book"
	       "\\documentclass{book}"
	       ("\\part{%s}" . "\\part*{%s}")
	       ("\\chapter{%s}" . "\\chapter*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	     )

(add-to-list 'org-latex-classes
	     '("book-np"
	       "\\documentclass{book}"
	       ("\\chapter{%s}" . "\\chapter*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	     )

(add-to-list 'org-latex-classes
	     '("report"
	       "\\documentclass{report}"
	       ("\\part{%s}" . "\\part*{%s}")
	       ("\\chapter{%s}" . "\\chapter*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	     )

(add-to-list 'org-latex-classes
	     '("extbook"
	       "\\documentclass{extbook}"
	       ("\\part{%s}" . "\\part*{%s}")
	       ("\\chapter{%s}" . "\\chapter*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	     )

(add-to-list 'org-latex-classes
	     '("extarticle"
	       "\\documentclass{extarticle}"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	     )

(add-to-list 'org-latex-classes
	     '("exam"
	       "\\documentclass[letterpaper,12pt]{exam}"
	       ("\\titledquestion{%s}" . "\\titledquestion*{%s}")
	       ("\\part{%s}" . "\\part*{%s}")
	       ("\\subpart{%s}" . "\\subpart*{%s}")
	       ("\\subsubpart{%s}" . "\\subsubpart*{%s}"))
	     )

(add-to-list 'org-latex-classes
	     '("ws-mpla"
	       "\\documentclass{ws-mpla}"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	     )

(add-to-list 'org-latex-classes
             '("usm-thesis"
               "\\documentclass{usm-thesis}"
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
             )

(add-to-list 'org-latex-classes
             '("revtex4-1"
               "\\documentclass{revtex4-1}
[NO-DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
)

(add-to-list 'org-latex-classes
             '("revtex4-2"
               "\\documentclass{revtex4-2}
[NO-DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
)

(add-to-list 'org-latex-classes
             '("iopart"
               "\\documentclass{iopart}
[NO-DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
)

(add-to-list 'org-latex-classes
             '("epjc"
               "\\documentclass[epjc3]{svjour3}
[NO-DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
)

(add-to-list 'org-latex-classes
             '("elsarticle"
               "\\documentclass{elsarticle}
[NO-DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
)

(use-package org-download
  :after org
  :bind
  (:map org-mode-map
        (("s-Y" . org-download-screenshot)
         ("s-y" . org-download-yank)))
  :custom
  (org-download-screenshot-method "spectacle -b -n -r -o %s")
  (org-download-image-dir "assets/"))

(use-package calfw
  :ensure t
  :config
  (use-package calfw-org
    :ensure t
    :config (setq cfw:org-agenda-schedule-args '(:timestamp)
                  cfw:org-overwrite-default-keybinding t)  
    )
  )

(use-package org-elp
  :config
  (setq org-elp-idle-time 0.5
        org-elp-split-fraction 0.25))

(setq org-hide-emphasis-markers t)

(use-package org-pdftools
  :hook (org-mode . org-pdftools-setup-link))

(add-to-list 'org-file-apps '("\\.pdf\\'" . (lambda (file link) (org-pdftools-open link))))

(setq
   org_notes (concat (getenv "HOME") "/Documents/org-roam")
   my_bib (concat (getenv "HOME") "/Documents/LaTeXFiles/References.bib")
   org-directory org_notes
   deft-directory org_notes
   org-roam-directory org_notes)

(use-package deft
  :config
  (setq deft-directory org_notes
        deft-recursive t
        deft-strip-summary-regexp ":PROPERTIES:\n\\(.+\n\\)+:END:\n"
        deft-use-filename-as-title t)
  :bind
  ("C-c n e" . deft))

(use-package bibtex-completion)

(use-package helm-bibtex)

(use-package ivy-bibtex
  :init
  (setq bibtex-completion-notes-path "~/Documents/org-roam/refs/"
        bibtex-completion-bibliography '("~/Documents/LaTeXFiles/References.bib")
        bibtex-completion-library-path '("~/Documents/org-roam/refs/")
        bibtex-completion-pdf-field "file"
        bibtex-completion-pdf-symbol "⌘"
        bibtex-completion-notes-symbol "✎"
        bibtex-completion-additional-search-fields '(keywords)
        
	bibtex-completion-display-formats
	'((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
	  (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
	  (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	  (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	  (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
        ;; bibtex-completion-notes-template-multiple-files
        ;; (concat
        ;;  "#+TITLE: ${title}\n"
        ;;  "#+ROAM_KEY: cite:${citekey}\n"
        ;;  "* TODO Notes\n"
        ;;  ":PROPERTIES:\n"
        ;;  ":Custom_ID: ${citekey}\n"
        ;;  ":NOTER_DOCUMENT: %(orb-process-file-field \"${citekey}\")\n"
        ;;  ":AUTHOR: ${author-abbrev}\n"
        ;;  ":JOURNAL: ${journaltitle}\n"
        ;;  ":DATE: ${date}\n"
        ;;  ":YEAR: ${year}\n"
        ;;  ":DOI: ${doi}\n"
        ;;  ":URL: ${url}\n"
        ;;  ":END:\n\n"
        ;;  )
        )
  )

;; Show key bindings
(defun scimax-show-key-bindings ()
  "Show keys in the map"
  (interactive)
  (describe-keymap ivy-minibuffer-map))

(define-key ivy-minibuffer-map (kbd "C-?") #'scimax-show-key-bindings)

(defun scimax-ivy-show-marked-candidates ()
  "Show marked candidates"
  (interactive)
  (with-help-window "*ivy-marked-candidates*"
    (cl-loop for cand in ivy-marked-candidates
	     do
	     (princ (s-trim cand))
	     (princ "\n"))))

(define-key ivy-minibuffer-map (kbd "C-s") #'scimax-ivy-show-marked-candidates)

;; Marking candidates
(defun scimax-ivy-toggle-mark ()
  "Toggle the mark"
  (interactive)
  (if (ivy--marked-p)
      (ivy-unmark)
    (ivy-mark))
  (ivy-previous-line))

(define-key ivy-minibuffer-map (kbd "C-SPC")
  #'scimax-ivy-toggle-mark)

(use-package org-ref
  :init
  (require 'bibtex)
  (setq bibtex-autokey-year-length 2
	bibtex-autokey-name-year-separator ""
	bibtex-autokey-year-title-separator "_"
	bibtex-autokey-titleword-separator "_"
	bibtex-autokey-titlewords 4
	bibtex-autokey-titlewords-stretch 1
	bibtex-autokey-titleword-length 5)
  (define-key bibtex-mode-map (kbd "H-b") 'org-ref-bibtex-hydra/body)
  (define-key org-mode-map (kbd "C-c ]") 'org-ref-cite-insert-ivy)
  (define-key org-mode-map (kbd "C-c }") 'org-ref-insert-ref-link)
  (define-key org-mode-map (kbd "s-[") 'org-ref-inser-link-hydra/body)

  (require 'org-ref-ivy)
  (setq org-ref-insert-link-function 'org-ref-insert-link-hydra/body
        org-ref-insert-cite-function 'org-ref-cite-insert-ivy
        org-ref-insert-label-function 'org-ref-insert-label-link
        org-ref-insert-ref-function 'org-ref-insert-ref-link
        org-ref-cite-onclick-function (lambda (_) (org-ref-citation-hydra/body)))
  (require 'org-ref-arxiv)
  (require 'org-ref-scopus)
  (require 'org-ref-wos)
  (require 'org-ref-isbn)
  (require 'doi-utils)
  (require 'org-ref-pdf)

  :config
  (setq
   org-ref-completion-library 'org-ref-ivy-cite
   org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
   org-ref-default-bibliography (list my_bib) ;; (list "/home/haozeke/GDrive/zotLib.bib")
   org-ref-bibliography-notes (concat (getenv "HOME") "/Documents/org-roam/refs/")
   org-ref-note-title-format "* TODO %y - %t\n :PROPERTIES:\n  :Custom_ID: %k\n  :NOTER_DOCUMENT: %F\n :ROAM_KEY: cite:%k\n  :AUTHOR: %9a\n  :JOURNAL: %j\n  :YEAR: %y\n  :VOLUME: %v\n  :PAGES: %p\n  :DOI: %D\n  :URL: %U\n :END:\n\n"
   org-ref-notes-directory "~/Documents/org-roam/refs/"
   org-ref-pdf-directory "/home/oscar/Documents/org-roam/refs/"
   org-ref-notes-function 'orb-edit-notes))

;;;; ADDED AFTER THE UPDATE OF org-ref v.3
;; (require 'bibtex)
;; 
;; (setq bibtex-autokey-year-length 4
;; 	bibtex-autokey-name-year-separator "-"
;; 	bibtex-autokey-year-title-separator "-"
;; 	bibtex-autokey-titleword-separator "-"
;; 	bibtex-autokey-titlewords 2
;; 	bibtex-autokey-titlewords-stretch 1
;; 	bibtex-autokey-titleword-length 5
;; 	org-ref-bibtex-hydra-key-binding (kbd "H-b"))
;; 
;; (define-key bibtex-mode-map (kbd "H-b") 'org-ref-bibtex-hydra/body)
;; 
;; (require 'org-ref-helm)
;; (define-key org-mode-map (kbd "C-c ]") 'org-ref-insert-link)

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/Documents/org-roam")
  (org-roam-completion-everywhere t)
  :bind (("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n r" . org-roam-node-random)		    
         (:map org-mode-map
               (("C-c n i" . org-roam-node-insert)
                ("C-c n o" . org-id-get-create)
                ("C-c n t" . org-roam-tag-add)
                ("C-c n a" . org-roam-alias-add)
                ("C-c n l" . org-roam-buffer-toggle)))
         :map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  ;; (("C-c n l" . org-roam-buffer-toggle)
  ;;  ("C-c n f" . org-roam-node-find)
  ;;  ("C-c n i" . org-roam-node-insert)
  ;;  :map org-mode-map
  ;;  ("C-M-i"    . completion-at-point))
  :config
  (require 'org-roam-dailies)
  (org-roam-setup)
  (org-roam-db-autosync-mode)
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-direction)
                 (direction . right)
                 (window-width . 0.33)
                 (window-height . fit-window-to-buffer))))

(setq org-roam-capture-templates
      '(("d" "default" plain
         "%?"
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
         :unnarrowed t)
        ("b" "book notes" plain
         (file "~/Documents/org-roam/refs/templates/biblio.org")
         :if-new (file+head "refs/${slug}.org" "#+title: ${title}\n#+date: %U\n")
         :unnarrowed t)
        ("p" "project" plain
         (file "~/Documents/org-roam/templates/project-template.org")
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n#+filetags: project\n\n")
         :unnarrowed t)
        ("m" "minute" plain
         (file "~/Documents/org-roam/templates/minute-template.org")
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n#+author: %n\n#+options: toc:nil author:nil date:nil num:nil\n#+filetags: minute\n\n")
         :unnarrowed t)
        ))

(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))
(define-key org-mode-map (kbd "C-c n I") 'org-roam-node-insert-immediate)

(defun my/org-roam-capture-inbox ()
  (interactive)
  (org-roam-capture- :node (org-roam-node-create)
                     :templates '(("i" "inbox" plain "* %?"
                                  :if-new (file+head "inbox.org" "#+title: Inbox\n")))))

(global-set-key (kbd "C-c n b") #'my/org-roam-capture-inbox)

(setq org-roam-dailies-directory "journal/")

(setq org-roam-dailies-capture-templates
      '(("d" "default" entry "* %<%I:%M %p>: %?"
         :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))

(defun my/org-roam-copy-todo-to-today ()
  (interactive)
  (let ((org-refile-keep t) ;; Set this to nil to delete the original!
        (org-roam-dailies-capture-templates
          '(("t" "tasks" entry "%?"
             :if-new (file+head+olp "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n" ("Tasks")))))
        (org-after-refile-insert-hook #'save-buffer)
        today-file
        pos)
    (save-window-excursion
      (org-roam-dailies--capture (current-time) t)
      (setq today-file (buffer-file-name))
      (setq pos (point)))

    ;; Only refile if the target file is different than the current file
    (unless (equal (file-truename today-file)
                   (file-truename (buffer-file-name)))
      (org-refile nil nil (list "Tasks" today-file nil pos)))))

(add-to-list 'org-after-todo-state-change-hook
             (lambda ()
               (when (equal org-state "DONE")
                 (my/org-roam-copy-todo-to-today))))

(use-package org-roam-bibtex
  :after (org-roam)
  :diminish org-roam-bibtex-mode
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :bind (:map org-mode-map
         (("C-c n a" . orb-note-actions)))
  :config
  (setq org-roam-bibtex-preformat-keywords
        '("citekey" "title" "url" "file" "author-or-editor" "keywords"))
  (setq orb-templates
        '(("r" "ref" plain
           (function org-roam-capture--get-point)
           ""
           :file-name "refs/${slug}"
           :head "#+TITLE: ${citekey}: ${title}
#+ROAM_KEY: ${ref}
#+created: %U
#+last_modified: %U\n\n"
           :unnarrowed t)
          ("n" "ref + noter" plain
           (function org-roam-capture--get-point)
           ""
           :file-name "refs/${citekey}"
           :head "#+TITLE: ${citekey}: ${title}
#+ROAM_KEY: ${ref}
#+created: %U

- tags ::
- keywords :: ${keywords}

* ${title}
:PROPERTIES:
:CUSTOM_ID: ${citekey}
:URL: ${url}
:AUTHOR: ${author-or-editor}
:END:

* Notes :noter:
:PROPERTIES:
:NOTER_DOCUMENT: ${citekey}.pdf
:NOTER_PAGE:
:END:\n\n"

           :unnarrowed t))))

(use-package org-roam-ui)

(use-package org-roam-bibtex
  :after (org-roam)
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :bind (:map org-mode-map
              (("C-c n a" . orb-note-actions)))
  :init
  :custom
  (setq orb-preformat-keywords
        '("citekey" "title" "url" "author-or-editor" "keywords" "file")
        orb-process-file-keyword t
        orb-attached-file-extensions '("pdf"))

  (setq org-roam-capture-templates
        '(("r" "bibliography reference" plain
           (file "~/Documents/org-roam/refs/templates/biblio.org")
           :target
           (file+head "references/${citekey}.org" "#+title: ${title}\n"))))
  )

(use-package org-noter
  :after
  (:any org pdf-view)
  :bind 
  ("<f8>" . org-noter)
  ("<f9>" . org-noter-create-skeleton)
  :config
  (setq ;; org-noter-notes-window-location 'other-frame ;; The WM can handle splits
        ;; Please stop opening frames
        ;; org-noter-always-create-frame nil
        ;; I want to see the whole file
        org-noter-hide-other nil
        ;; Everything is relative to the main notes file
        ;; org-noter-notes-search-path org_notes
        )
  (require 'org-noter-pdftools))

(use-package org-noter-pdftools
  :after org-noter
  :config
  ;; Add a function to ensure precise note is inserted
  (defun org-noter-pdftools-insert-precise-note (&optional toggle-no-questions)
    (interactive "P")
    (org-noter--with-valid-session
     (let ((org-noter-insert-note-no-questions (if toggle-no-questions
                                                   (not org-noter-insert-note-no-questions)
                                                 org-noter-insert-note-no-questions))
           (org-pdftools-use-isearch-link t)
           (org-pdftools-use-freestyle-annot t))
       (org-noter-insert-note (org-noter--get-precise-info)))))

  ;; fix https://github.com/weirdNox/org-noter/pull/93/commits/f8349ae7575e599f375de1be6be2d0d5de4e6cbf
  (defun org-noter-set-start-location (&optional arg)
    "When opening a session with this document, go to the current location.
With a prefix ARG, remove start location."
    (interactive "P")
    (org-noter--with-valid-session
     (let ((inhibit-read-only t)
           (ast (org-noter--parse-root))
           (location (org-noter--doc-approx-location (when (called-interactively-p 'any) 'interactive))))
       (with-current-buffer (org-noter--session-notes-buffer session)
         (org-with-wide-buffer
          (goto-char (org-element-property :begin ast))
          (if arg
              (org-entry-delete nil org-noter-property-note-location)
            (org-entry-put nil org-noter-property-note-location
                           (org-noter--pretty-print-location location))))))))
  (with-eval-after-load 'pdf-annot
    (add-hook 'pdf-annot-activate-handler-functions #'org-noter-pdftools-jump-to-note)))

(defconst cdlatex-math-modify-alist-default
  '(
    ( ?\.   "\\dot"               nil        t   t   nil )
    ( ?\:   "\\ddot"              nil        t   t   nil )
    ( ?\~   "\\tilde"             nil        t   t   nil )
    ( ?N    "\\widetilde"         nil        t   t   nil )
    ( ?^    "\\hat"               nil        t   t   nil )
    ( ?H    "\\widehat"           nil        t   t   nil )
    ( ?\-   "\\bar"               nil        t   t   nil )
    ( ?T    "\\overline"          nil        t   nil nil )
    ( ?\_   "\\underline"         nil        t   nil nil )
    ( ?\{   "\\overbrace"         nil        t   nil nil )
    ( ?\}   "\\underbrace"        nil        t   nil nil )
    ( ?\<   "\\vec"               nil        t   t   nil )
    ( ?\>   "\\vec"               nil        t   t   nil )
    ( ?/    "\\grave"             nil        t   t   nil )
    ( ?\\   "\\acute"             nil        t   t   nil )
    ( ?v    "\\check"             nil        t   t   nil )
    ( ?u    "\\breve"             nil        t   t   nil )
    ( ?m    "\\mbox"              nil        t   nil nil )
    ( ?c    "\\mathcal"           nil        t   nil nil )
    ( ?r    "\\mathrm"            "\\textrm" t   nil nil )
    ( ?i    "\\mathit"            "\\textit" t   nil nil )
    ( ?l    nil                   "\\textsl" t   nil nil )
    ( ?b    "\\mathbf"            "\\textbf" t   nil nil )
    ( ?e    "\\mathem"            "\\emph"   t   nil nil )
    ( ?y    "\\mathtt"            "\\texttt" t   nil nil )
    ( ?f    "\\mathsf"            "\\textsf" t   nil nil )
    ( ?g    "\\mathfrak"          nil        t   nil nil )
    ( ?k    "\\mathbb"            nil        t   nil nil )
    ( ?0    "\\textstyle"         nil        nil nil nil )
    ( ?1    "\\displaystyle"      nil        nil nil nil )
    ( ?2    "\\scriptstyle"       nil        nil nil nil )
    ( ?3    "\\scriptscriptstyle" nil        nil nil nil )
    )
  "Default for cdlatex-math-modify-alist.")

(defconst cdlatex-math-symbol-alist-default
  '(
    ( ?a  ("\\alpha"          ))
    ( ?A  ("\\forall"         "\\aleph"))
    ( ?b  ("\\beta"           ))
    ( ?B  (""                 ))
    ( ?c  (""                 ""                "\\cos"))
    ( ?C  (""                 ""                "\\arccos"))
    ( ?d  ("\\delta"          "\\partial"))
    ( ?D  ("\\Delta"          "\\nabla"))
    ( ?e  ("\\epsilon"        "\\varepsilon"    "\\exp"))
    ( ?E  ("\\exists"         ""                "\\ln"))
    ( ?f  ("\\phi"            "\\varphi"))
    ( ?F  ("\\Phi"                 ))
    ( ?g  ("\\gamma"          ""                "\\lg"))
    ( ?G  ("\\Gamma"          ""                "10^{?}"))
    ( ?h  ("\\eta"            "\\hbar"))
    ( ?H  (""                 ))
    ( ?i  ("\\in"             "\\imath"))
    ( ?I  (""                 "\\Im"))
    ( ?j  (""                 "\\jmath"))
    ( ?J  (""                 ))
    ( ?k  ("\\kappa"          ))
    ( ?K  (""                 ))
    ( ?l  ("\\lambda"         "\\ell"           "\\log"))
    ( ?L  ("\\Lambda"         ))
    ( ?m  ("\\mu"             ))
    ( ?M  (""                 ))
    ( ?n  ("\\nu"             ""                "\\ln"))
    ( ?N  ("\\nabla"          ""                "\\exp"))
    ( ?o  ("\\omega"          ))
    ( ?O  ("\\Omega"          "\\mho"))
    ( ?p  ("\\pi"             "\\varpi"))
    ( ?P  ("\\Pi"             ))
    ( ?q  ("\\theta"          "\\vartheta"))
    ( ?Q  ("\\Theta"          ))
    ( ?r  ("\\rho"            "\\varrho"))
    ( ?R  (""                 "\\Re"))
    ( ?s  ("\\sigma"          "\\varsigma"      "\\sin"))
    ( ?S  ("\\Sigma"          ""                "\\arcsin"))
    ( ?t  ("\\tau"            ""                "\\tan"))
    ( ?T  (""                 ""                "\\arctan"))
    ( ?u  ("\\upsilon"        ))
    ( ?U  ("\\Upsilon"        ))
    ( ?v  ("\\vee"            ))
    ( ?V  ("\\Phi"            ))
    ( ?w  ("\\xi"             ))
    ( ?W  ("\\Xi"             ))
    ( ?x  ("\\chi"            ))
    ( ?X  (""                 ))
    ( ?y  ("\\psi"            ))
    ( ?Y  ("\\Psi"            ))
    ( ?z  ("\\zeta"           ))
    ( ?Z  (""                 ))
    ( ?   (""                 ))
    ( ?0  ("\\emptyset"       ))
    ( ?1  (""                 ))
    ( ?2  (""                 ))
    ( ?3  (""                 ))
    ( ?4  (""                 ))
    ( ?5  (""                 ))
    ( ?6  (""                 ))
    ( ?7  (""                 ))
    ( ?8  ("\\infty"          ))
    ( ?9  (""                 ))
    ( ?!  ("\\neg"            ))
    ( ?@  (""                 ))
    ( ?#  (""                 ))
    ( ?$  (""                 ))
    ( ?%  (""                 ))
    ( ?^  ("\\uparrow"        ))
    ( ?&  ("\\wedge"          ))
    ( ?\? (""                 ))
    ( ?~  ("\\approx"         "\\simeq"))
    ( ?_  ("\\downarrow"      ))
    ( ?+  ("\\cup"            ))
    ( ?-  ("\\leftrightarrow" "\\longleftrightarrow" ))
    ( ?*  ("\\times"          ))
    ( ?/  ("\\not"            ))
    ( ?|  ("\\mapsto"         "\\longmapsto"))
    ( ?\\ ("\\setminus"       ))
    ( ?=  ("\\Leftrightarrow" "\\Longleftrightarrow"))
    ( ?\( ("\\langle"         ))
    ( ?\) ("\\rangle"         ))
    ( ?\[ ("\\Leftarrow"      "\\Longleftarrow"))
    ( ?\] ("\\Rightarrow"     "\\Longrightarrow"))
    ( ?{  ("\\subset"         ))
    ( ?}  ("\\supset"         ))
    ( ?<  ("\\leftarrow"      "\\longleftarrow"     "\\min"))
    ( ?>  ("\\rightarrow"     "\\longrightarrow"    "\\max"))
    ( ?`  (""                 ))
    ( ?'  ("\\prime"          ))
    ( ?.  ("\\cdot"           ))
    )
  "Default for cdlatex-math-symbol-alist."
  )

(use-package projectile 
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
)

(use-package counsel-projectile
  :after projectile
  :config
  (counsel-projectile-mode 1))

(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e/")

(use-package sage-shell-mode
  :init
  (setq sage-shell:sage-root "/usr/bin")
  ;; (setq sage-shell:sage-executable "/usr/bin/sage")
  (sage-shell:define-alias)
  (add-hook 'sage-shell-mode-hook #'eldoc-mode)
  (add-hook 'sage-shell:sage-mode-hook #'eldoc-mode)
  :config
  (use-package ob-sagemath
    :config
    (setq org-babel-default-header-args:sage '((:session . t)
                                           (:results . "output")))
    )
  )

(use-package ein)

(setq erc-nick "OCFelisola")
(setq erc-prompt (lambda () (concat "[" (buffer-name) "]")))
(setq erc-hide-list '("JOIN" "PART" "QUIT"))
(setq erc-autojoin-channels-alist '(("#emacs" "#org-mode")))

(use-package erc-hl-nicks
  :config
  (erc-update-modules))

(use-package erc-image
  :config
  (add-to-list 'erc-modules 'image))

(use-package writegood-mode)

(use-package writeroom-mode
  :config
  (setq writeroom-width 0.9))

(setq elfeed-db-directory "~/.emacs.d/elfeeddb")

(use-package elfeed)

(setq elfeed-feeds
      '(("https://xkcd.com/rss.xml" comics)
        ("https://what-if.xkcd.com/feed.atom" comics)
        ("http://www.phdcomics.com/gradfeed.php" comics)
        ("http://nullprogram.com/feed" emacs)
        ("http://irreal.org/blog/?feed=rss2" emacs)
        ("http://pragmaticemacs.com/feed/" emacs)
        ("http://sachachua.com/blog/feed/" emacs sachachua)
        ("https://rgoswami.me/posts/index.xml" emacs)
        ("https://lucidmanager.org/index.xml" emacs)
        ("https://200ok.ch/atom.xml" emacs)
        ("https://pointieststick.com/feed/" linux)
        ("https://cadabra.science/qa/feed/qa.rss" cadabra)
        ("https://protonmail.com/blog/feed/" proton)
        ("http://arxiv.org/rss/gr-qc" arxiv gr-qc)
        ("http://arxiv.org/rss/math.MP" arxiv math-ph)
        ("http://arxiv.org/rss/physics.class-ph" arxiv class-ph)
        ("http://arxiv.org/rss/physics.ed-ph" arxiv ed-ph)
        ("http://arxiv.org/rss/physics.gen-ph" arxiv gen-ph)
        ("https://realpython.com/atom.xml" python real-python)
        ("https://api.quantamagazine.org/feed/" science)
        ;; ("https://opensource.com/feed" FOSS)
))

(defun ocf/elfeed-entry-to-arxiv ()
  "Fetch an arXiv paper into the local library from the current elfeed entry."
  (interactive)
  (let* ((link (elfeed-entry-link elfeed-show-entry))
         (match-idx (string-match "arxiv.org/abs/\\([0-9.]*\\)" link))
         (matched-arxiv-number (match-string 1 link)))
    (when matched-arxiv-number
      (message "Going to arXiv: %s" matched-arxiv-number)
      (arxiv-get-pdf-add-bibtex-entry matched-arxiv-number my_bib org-ref-pdf-directory)
      )))
(define-key elfeed-show-mode-map (kbd "a") 'ocf/elfeed-entry-to-arxiv)

(use-package nov
  ;; :hook (nov-mode . zp/variable-pitch-mode)
  :mode ("\\.\\(epub\\|mobi\\)\\'" . nov-mode))

(setq sql-sqlite-program "/usr/bin/sqlite3")
(setq calibredb-program "/opt/calibre/calibredb")

(use-package calibredb
  :defer t
  ;; :init
  ;; (autoload 'calibredb "calibredb")
  :config
  (setq calibredb-root-dir "~/Ebooks/Ebooks Collection")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (setq calibredb-library-alist '(("~/Ebooks/Ebooks Collection"))))

(use-package diffpdf
  :ensure t
  :config (require 'diffpdf))

(use-package focus)

(add-to-list 'focus-mode-to-thing '(python-mode . paragraph))

(use-package hide-mode-line)

(defun efs/presentation-setup ()
  ;; Hide the mode line
  (hide-mode-line-mode 1)

  ;; Display images inline
  (org-display-inline-images) ;; Can also use org-startup-with-inline-images

  ;; Scale the text.  The next line is for basic scaling:
  (setq text-scale-mode-amount 3)
  (setq-local org-format-latex-options (plist-put org-format-latex-options :scale 3.5))
  (text-scale-mode 1))

  ;; This option is more advanced, allows you to scale other faces too
  ;; (setq-local face-remapping-alist '((default (:height 2.0) variable-pitch)
  ;;                                    (org-verbatim (:height 1.75) org-verbatim)
  ;;                                    (org-block (:height 1.25) org-block))))

(defun efs/presentation-end ()
  ;; Show the mode line again
  (hide-mode-line-mode 0)

  ;; Turn off text scale mode (or use the next line if you didn't use text-scale-mode)
  (text-scale-mode 0)

  ;; If you use face-remapping-alist, this clears the scaling:
  (setq-local face-remapping-alist '((default variable-pitch default))))

(use-package org-tree-slide
  :hook ((org-tree-slide-play . efs/presentation-setup)
         (org-tree-slide-stop . efs/presentation-end))
  :custom
  (org-tree-slide-slide-in-effect t)
  (org-tree-slide-activate-message "Presentation started!")
  (org-tree-slide-deactivate-message "Presentation finished!")
  (org-tree-slide-header t)
  (org-tree-slide-breadcrumbs " > ")
  (org-image-actual-width nil))

(use-package academic-phrases)

(add-to-list 'package-pinned-packages '(telega . "melpa-stable"))
(setq telega-server-libs-prefix "/usr")
(use-package telega
  :ensure t
  :config
  (define-key global-map (kbd "C-c t") telega-prefix-map))

(use-package mastodon
  :ensure t
  :config
  (mastodon-discover)
  (setq mastodon-instance-url "https://mathstodon.xyz/"
        mastodon-active-user "@OCFelisola"))
